# Hubris - TypeScript Uber Hypermedia Client

Hypermedia client for interacting with the Uber hypermedia format.

[![pipeline status](https://gitlab.com/petejohanson/hubris/badges/master/pipeline.svg)](https://gitlab.com/petejohanson/hubris/commits/master)
[![code coverage](https://codecov.io/gl/petejohanson/hubris/branch/master/graph/badge.svg)](https://codecov.io/gl/petejohanson/hubris/branch/master/)

## Examples

```typescript
import 'isomorphic-fetch';
import { follow, parse, query, relMatch, IUberDocumeny } from 'hubris';


fetch('/people')
  .then(parse)
  .then(query(relMatch('search')))
  .then(data => follow(data, { q: 'John Doe' })
  .then(parse)
  .then((resultsDoc: IUberDocument) => console.log('Document', resultsDoc));

```
