import { Action, IData, IDataParent, IUberDocument, Transclude } from '../types';

import URI from 'urijs';
import URITemplate from 'urijs/src/URITemplate';
import MIMEType from 'whatwg-mimetype';

const parseMimeTypes = (s: string[]) => s.map(m => MIMEType.parse(m)).filter((m): m is MIMEType => !!m);

interface IJsonDataParent {
  data?: IJsonData[];
}

interface IJsonData extends IJsonDataParent {
  id?: string;
  name?: string;
  label?: string;
  rel?: string[];
  url?: string;
  model?: string;
  action?: Action;
  value?: string | number | boolean;
  transclude?: Transclude;
  templated?: string;
  accepting?: string[];
  sending?: string[];
}

interface IJsonUber extends IJsonDataParent {
  version?: string;
  error?: IJsonDataParent;
  data?: IJsonData[];
}

const mapData: (doc: IUberDocument) => (node: IJsonData) => IData = doc => node => {
  const { model: modelStr, url: urlStr, data: jsonData, accepting, sending, templated, ...attrs } = node;

  const ret: IData = { ...attrs, document: doc };

  if (urlStr) {
    ret.url = templated === 'true' ? new URITemplate(urlStr) : new URI(urlStr);
  }

  if (jsonData) {
    ret.data = jsonData.map(mapData(doc));
  }

  if (modelStr) {
    ret.model = new URITemplate(modelStr);
  }

  if (accepting) {
    ret.accepting = parseMimeTypes(accepting);
  }

  if (sending) {
    ret.sending = parseMimeTypes(sending);
  }

  return ret;
};

const mapUber: (node: IJsonUber) => IUberDocument = node => {
  const doc: IUberDocument = {};
  if (node.data) {
    doc.data = node.data.map(mapData(doc));
  }

  if (node.version) {
    doc.version = node.version;
  }

  if (node.error && node.error.data) {
    doc.error = { data: node.error.data.map(mapData(doc)) };
  }

  return doc;
};


export const parse = (r: Response) =>
  r.json().then(j => mapUber(j.uber));
