/* tslint:disable:no-non-null-assertion */
import URI, * as uri from 'urijs';
import URITemplate from 'urijs/src/URITemplate';
import MIMEType from 'whatwg-mimetype';

import { Action, IUberDocument } from '../types';
import { parse } from './xml';
/**
 * XML Parsing tests
 */

describe('XML parsing test', () => {
  describe('with the XML variant', () => {
    it('parses a valid Uber document', () => {
      const resp = new Response('<?xml version="1.0" encoding="UTF-8" ?><uber version="1.0"></uber>');

      return parse(resp).then(p => expect(p).toEqual({ version: '1.0' }));
    });

    it('parses a complete Uber XML document', () => {
      const body = `
        <?xml version="1.0" encoding="UTF-8" ?>
        <uber version="1.0">
          <data rel="self" url="http://example.org/" />
          <data name="list" label="ToDo List" rel="collection" url="http://example.org/list/"/>
          <data name="search" label="Search" rel="search collection" url="http://example.org/search{?title}" templated="true" />
          <data name="todo" rel="item http://example.org/rels/todo" url="http://example.org/list/1">
            <data name="title" label="Title">Clean House</data>
            <data name="dueDate" label="Date Due">2014-05-01</data>
          </data>
          <data name="todo" rel="item http://example.org/rels/todo" url="http://example.org/list/2">
            <data name="title" label="Title">Paint the fence</data>
            <data name="dueDate" label="Date Due">2014-06-01</data>
          </data>
        </uber>
        `.trim();

      const expectedDoc: IUberDocument = {
        version: '1.0',
        data: [
          {
            rel: ['self'],
            url: new URI('http://example.org/')
          },
          {
            name: 'list',
            label: 'ToDo List',
            rel: ['collection'],
            url: new URI('http://example.org/list/')
          },
          {
            name: 'search',
            label: 'Search',
            rel: ['search', 'collection'],
            url: new URITemplate('http://example.org/search{?title}'),
          },
          {
            name: 'todo',
            rel: ['item', 'http://example.org/rels/todo'],
            url: new URI('http://example.org/list/1'),
            data: [
              { name: 'title', label: 'Title', value: 'Clean House' },
              { name: 'dueDate', label: 'Date Due', value: '2014-05-01' }
            ]
          },
          {
            name: 'todo',
            rel: ['item', 'http://example.org/rels/todo'],
            url: new URI('http://example.org/list/2'),
            data: [
              { name: 'title', label: 'Title', value: 'Paint the fence' },
              { name: 'dueDate', label: 'Date Due', value: '2014-06-01' }
            ]
          }
        ]
      };
      const resp = new Response(body);

      return parse(resp).then(p => expect(p).toMatchObject(expectedDoc));
    });

    it('parses an Uber document with errors properly', () => {
      const body = `
        <uber version="1.0">
          <error>
            <data name="type"
              rel="https://example.com/rels/http-problem#type">
              out-of-credit
            </data>
            <data name="title"
              rel="https://example.com/rels/http-problem#title">
              You do not have enough credit
            </data>
            <data name="detail"
              rel="https://example.com/rels/http-problem#detail">
              Your balance is 30, but the cost is 50.
            </data>
            <data name="balance"
              rel="https://example.com/rels/http-problem#balance">
              30
            </data>
          </error>
        </uber>`.trim();

      const expectedDoc: IUberDocument = {
        'version': '1.0',
        'error':
        {
          'data' :
          [
            {
              'name' : 'type',
              'rel' : ['https://example.com/rels/http-problem#type'],
              'value' : 'out-of-credit'
            },
            {
              'name' : 'title',
              'rel' : ['https://example.com/rels/http-problem#title'],
              'value' : 'You do not have enough credit'
            },
            {
              'name' : 'detail',
              'rel' : ['https://example.com/rels/http-problem#detail'],
              'value' : 'Your balance is 30, but the cost is 50.'
            },
            {
              'name' : 'balance',
              'rel' : ['https://example.com/rels/http-problem#balance'],
              'value' : '30'
            }
          ]
        }
      };

      const resp = new Response(body);

      return parse(resp).then(p => {
        expect(p).toMatchObject(expectedDoc);
        expect(p!.error!.data!.length).toEqual(4);
      });
    });

    it('returns a model propertly as a URITemplate', () => {
      const body = `
        <uber version="1.0">
          <data name="create"
            rel="http://example.org/rels/create"
            url="http://example.org/people/"
            action="append"
            model="g={givenName}&amp;f={familyName}&amp;e={email}&amp;a={avatarUrl}">
          </data>
        </uber>`.trim();

      const expectedDoc: IUberDocument = {
        version: '1.0',
        data: [
          {
            name: 'create',
            rel: ['http://example.org/rels/create'],
            url: new URI('http://example.org/people/'),
            action: Action.Append,
            model: new URITemplate('g={givenName}&f={familyName}&e={email}&a={avatarUrl}')
          }
        ]
      };

      const resp = new Response(body);

      return parse(resp).then(p => expect(p).toMatchObject(expectedDoc));
    });

    it('parsing the sending and accepting propertyes as MIMEType instances', () => {
      const body = `
        <uber version="1.0">
          <data name="create"
            url="http://example.org/people/"
            sending="application/json application/vnd.uber+json"
            accepting="text/plain">
          </data>
        </uber>`.trim();

      const expectedDoc: IUberDocument = {
        version: '1.0',
        data: [
          {
            name: 'create',
            url: new URI('http://example.org/people/'),
            sending: [new MIMEType('application/json'), new MIMEType('application/vnd.uber+json')],
            accepting: [new MIMEType('text/plain')]
          }
        ]
      };

      const resp = new Response(body);

      return parse(resp).then(p => expect(p).toMatchObject(expectedDoc));
    });

    it('returns an error for an invalid XML document', () => {
      return expect(parse(new Response('<party></party>'))).rejects.toBeInstanceOf(Error);
    });
  });
});
