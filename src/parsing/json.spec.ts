/* tslint:disable:no-non-null-assertion */
import URI, * as uri from 'urijs';
import URITemplate from 'urijs/src/URITemplate';
import MIMEType from 'whatwg-mimetype';

import { Action, IUberDocument } from '../types';
import { parse } from './json';
/**
 * JSON Parsing tests
 */

describe('JSON parsing test', () => {
  it('parses a valid Uber document', () => {
    const s = '{"uber":{"version":"1.0"}}';
    const resp = new Response(s);
    const expectedDoc: IUberDocument = { version: '1.0' };

    return parse(resp).then(p => expect(p).toMatchObject(expectedDoc));
  });

  it('parses a complete sample Uber document', () => {
    const body = `
      {
        "uber":
        {
          "version" : "1.0",
          "data" :
          [
            {
              "rel" : ["self"],
              "url" : "http://example.org/"
            },
            {
              "name" : "list",
              "label" : "ToDo List",
              "rel" : ["collection"],
              "url" : "http://example.org/list/"
            },
            {
              "name" : "search",
              "label" : "Search",
              "rel" : ["search","collection"],
              "url" : "http://example.org/search{?title}",
              "templated" : "true"
            },
            {
              "name" : "todo",
              "rel" : ["item","http://example.org/rels/todo"],
              "url" : "http://example.org/list/1",
              "data" :
              [
                {"name" : "title", "label" : "Title", "value" : "Clean House"},
                {"name" : "dueDate", "label" : "Date Due", "value" : "2014-05-01"}
              ]
            },
            {
              "name" : "todo",
              "rel" : ["item","http://example.org/rels/todo"],
              "url" : "http://example.org/list/2",
              "data" :
              [
                {"name" : "title", "label" : "Title", "value" : "Paint the fence"},
                {"name" : "dueDate", "label" : "Date Due", "value" : "2014-06-01"}
              ]
            }
          ]
        }
      }`.trim();

    const expectedDoc: IUberDocument = {
      version: '1.0',
      data: [
        {
          rel: ['self'],
          url: new URI('http://example.org/')
        },
        {
          name: 'list',
          label: 'ToDo List',
          rel: ['collection'],
          url: new URI('http://example.org/list/')
        },
        {
          name: 'search',
          label: 'Search',
          rel: ['search', 'collection'],
          url: new URITemplate('http://example.org/search{?title}'),
        },
        {
          name: 'todo',
          rel: ['item', 'http://example.org/rels/todo'],
          url: new URI('http://example.org/list/1'),
          data: [
            { name: 'title', label: 'Title', value: 'Clean House' },
            { name: 'dueDate', label: 'Date Due', value: '2014-05-01' }
          ]
        },
        {
          name: 'todo',
          rel: ['item', 'http://example.org/rels/todo'],
          url: new URI('http://example.org/list/2'),
          data: [
            { name: 'title', label: 'Title', value: 'Paint the fence' },
            { name: 'dueDate', label: 'Date Due', value: '2014-06-01' }
          ]
        }
      ]
    };

    const resp = new Response(body);

    return parse(resp).then(p => expect(p).toMatchObject(expectedDoc));
  });

  it('returns a model property as a URITemplate', () => {
    const body = `
      {
        "uber": {
          "version": "1.0",
          "data": [
            {
              "name": "create",
              "rel": ["http://example.org/rels/create"],
              "url": "http://example.org/people/",
              "action": "append",
              "model": "g={givenName}&f={familyName}&e={email}&a={avatarUrl}"
            }
          ]
        }
      }`.trim();

    const expectedDoc: IUberDocument = {
      version: '1.0',
      data: [
        {
          name: 'create',
          rel: ['http://example.org/rels/create'],
          url: new URI('http://example.org/people/'),
          action: Action.Append,
          model: new URITemplate('g={givenName}&f={familyName}&e={email}&a={avatarUrl}')
        }
      ]
    };

    const resp = new Response(body);

    return parse(resp).then(p => expect(p).toMatchObject(expectedDoc));
  });

  it('parsing the sending and accepting properties as MIMEType instances', () => {
    const body = `
      {
        "uber": {
          "version": "1.0",
          "data": [
            {
              "name": "create",
              "url": "http://example.org/people/",
              "sending": ["application/json"],
              "accepting": ["text/plain"]
            }
          ]
        }
      }`.trim();

    const expectedDoc: IUberDocument = {
      version: '1.0',
      data: [
        {
          name: 'create',
          url: new URI('http://example.org/people/'),
          sending: [new MIMEType('application/json')],
          accepting: [new MIMEType('text/plain')]
        }
      ]
    };

    const resp = new Response(body);

    return parse(resp).then(p => expect(p).toMatchObject(expectedDoc));
  });

  it('parses an Uber document with errors properly', () => {
    const body = `
      {
        "uber" :
        {
          "version" : "1.0",
          "error" :
          {
            "data" :
            [
              {
                "name" : "type",
                "rel" : ["https://example.com/rels/http-problem#type"],
                "value" : "out-of-credit"
              },
              {
                "name" : "title",
                "rel" : ["https://example.com/rels/http-problem#title"],
                "value" : "You do not have enough credit"
              },
              {
                "name" : "detail",
                "rel" : ["https://example.com/rels/http-problem#detail"],
                "value" : "Your balance is 30, but the cost is 50."
              },
              {
                "name" : "balance",
                "rel" : ["https://example.com/rels/http-problem#balance"],
                "value" : "30"
              }
            ]
          }
        }
      }`.trim();

    const expectedDoc: IUberDocument = {
      'version': '1.0',
      'error':
      {
        'data' :
        [
          {
            'name' : 'type',
            'rel' : ['https://example.com/rels/http-problem#type'],
            'value' : 'out-of-credit'
          },
          {
            'name' : 'title',
            'rel' : ['https://example.com/rels/http-problem#title'],
            'value' : 'You do not have enough credit'
          },
          {
            'name' : 'detail',
            'rel' : ['https://example.com/rels/http-problem#detail'],
            'value' : 'Your balance is 30, but the cost is 50.'
          },
          {
            'name' : 'balance',
            'rel' : ['https://example.com/rels/http-problem#balance'],
            'value' : '30'
          }
        ]
      }
    };

    const resp = new Response(body);

    return parse(resp).then(p => {
      expect(p).toMatchObject(expectedDoc);
      expect(p!.error!.data!.length).toEqual(4);
    });
  });
});
