import 'isomorphic-fetch';
import * as jestFetchMock from 'jest-fetch-mock';
import URI from 'urijs';

import { IUberDocument, UberError } from '../types';
import { parseResponse as parse } from './index';

/**
 * Parsing tests
 */

describe('Parsing test', () => {
  describe('with the JSON variant', () => {
    it('parses a valid Uber document', () => {
      const s = '{"uber":{"version":"1.0"}}';
      const resp = new Response(s, { headers: new Headers({ 'content-type': 'application/vnd.uber+json' }) });
      const expectedDoc: IUberDocument = { version: '1.0' };

      return parse(resp).then(p => expect(p).toEqual(expectedDoc));
    });

    it('sets the contextUri from the response', () => {
      jestFetchMock.mockResponseOnce('{"uber":{"version":"1.0"}}', { url: 'http://localhost/' });

      const expectedDoc: IUberDocument = { version: '1.0', contextUri: new URI('http://localhost/') };

      return fetch('http://localhost')
        .then(parse)
        .then(p => expect(p).toMatchObject(expectedDoc));
    });

    it('sets the document on all data elements', () => {
      const s = '{"uber":{"version":"1.0", "data": [{"id": "foo"}]}}';

      return parse(new Response(s)).then(p => (p.data || []).forEach(d => expect(d.document).toMatchObject(p)));
    });
  });

  describe('with the XML variant', () => {
    it('parses a valid Uber document', () => {
      const headers = new Headers({
        'Content-Type': 'application/vnd.uber+xml'
      });
      const resp = new Response('<?xml version="1.0" encoding="UTF-8" ?><uber version="1.0"></uber>', { headers });

      return parse(resp).then(p => expect(p).toEqual({ version: '1.0' }));
    });

    it('parses a valid Uber document with a parameterized content type', () => {
      const headers = new Headers({
        'Content-Type': 'application/vnd.uber+xml; profile=person'
      });
      const resp = new Response('<?xml version="1.0" encoding="UTF-8" ?><uber version="1.0"></uber>', { headers });

      return parse(resp).then(p => expect(p).toEqual({ version: '1.0' }));
    });

    it('sets the document on all data elements', () => {
      const headers = new Headers({ 'Content-Type': 'application/vnd.uber+xml' });
      const resp = new Response('<?xml version="1.0" encoding="UTF-8" ?><uber version="1.0"><data/></uber>', {
        headers
      });

      return parse(resp).then(p => (p.data || []).forEach(d => expect(d.document).toBe(p)));
    });
  });

  describe('rejecting the promise when parsing returns errors', () => {
    it('rejects the promise with a UberError when detecting error data', () => {
      const s = '{"uber":{"version":"1.0", "error": { "data": [ { "id": "name", "value": "Name field is required" } ] }}}';
      const resp = new Response(s);
      const expectedDoc: IUberDocument = { version: '1.0' };

      return expect(parse(resp)).rejects.toBeInstanceOf(UberError);
    });
  });
});
