import parseXml, { Document, Element, Text } from '@rgrove/parse-xml';
import URI from 'urijs';
import URITemplate from 'urijs/src/URITemplate';
import MIMEType from 'whatwg-mimetype';

import { IData, IDataParent, IUberDocument } from '../types';

let mapDataChildren: (document: IUberDocument) => (node: Element) => IData[] | undefined;

const parseMimeTypes = (s: string) =>
  s
    .split(' ')
    .map(m => MIMEType.parse(m))
    .filter((m): m is MIMEType => !!m);

const mapData: (doc: IUberDocument) => (node: Element) => IData = document => element => {
  const { attributes, children } = element;

  const { rel: relStr, model: modelStr, url: urlStr, templated, accepting, sending, ...attrs } = attributes;

  const ret: IData = { ...attrs, document };

  if (relStr) {
    ret.rel = relStr.split(' ');
  }

  if (urlStr) {
    ret.url = templated === 'true' ? new URITemplate(urlStr) : new URI(urlStr);
  }

  if (modelStr) {
    ret.model = new URITemplate(modelStr);
  }

  ret.data = mapDataChildren(document)(element);

  const valueNode = children.filter(n => n.type === 'text')[0] as Text;
  if (valueNode && valueNode.text.trim() !== '') {
    ret.value = valueNode.text.trim();
  }

  if (accepting) {
    ret.accepting = parseMimeTypes(accepting);
  }

  if (sending) {
    ret.sending = parseMimeTypes(sending);
  }

  return ret;
};

mapDataChildren = (doc: IUberDocument) => ({ children }) => {
  const elements = children.filter(n => n.type === 'element').map(n => n as Element);

  const childData = elements.filter(e => e.name === 'data').map(mapData(doc));

  return childData.length > 0 ? childData : undefined;
};

const mapError: (doc: IUberDocument) => (node: Element) => IDataParent = doc => node => ({
  data: mapDataChildren(doc)(node)
});

const mapUber: (node: Element) => IUberDocument = node => {
  const uber: IUberDocument = { ...node.attributes };

  const data = mapDataChildren(uber)(node);

  uber.data = data;

  const errors = node.children
    .filter(n => n.type === 'element')
    .map(n => n as Element)
    .filter(e => e.name === 'error')
    .map(mapError(uber));

  if (errors.length > 0) {
    uber.error = errors[0];
  }

  return uber;
};

const mapDocument: (doc: Document) => IUberDocument = ({ children }) => {
  const uberElement = children
    .filter(e => e.type === 'element')
    .map(e => e as Element)
    .filter(e => e.name === 'uber')[0];

  if (!uberElement) {
    throw new Error("Unable to located expected root 'uber' XML element.");
  }

  return mapUber(uberElement);
};

export const parse = (r: Response) => {
  return r
    .text()
    .then(parseXml)
    .then(mapDocument);
};
