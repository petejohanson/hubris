import MIMEType from 'whatwg-mimetype';

import { IUberDocument, UberError } from '../types';
import { parse as parseJson } from './json';
import { parse as parseXml } from './xml';

export { parseJson, parseXml };

import URI from 'urijs';

const standardParserSelector = (contentType?: MIMEType) => {
  if (!contentType) {
    return parseJson;
  }

  switch (contentType.essence) {
    case 'application/vnd.uber+xml':
      return parseXml;
    default:
      return parseJson;
  }
};

function setContextUri(r: Response) {
  return (d: IUberDocument) => {
    if (r.url && r.url !== '') {
      d.contextUri = new URI(r.url);
    }

    return d;
  };
}

function rejectIfError(doc: IUberDocument) {
  if (doc.error && doc.error.data && doc.error.data.length) {
    throw new UberError(doc);
  } else {
    return Promise.resolve(doc);
  }
}

export type DocumentParser = (response: Response) => Promise<IUberDocument>;

export function parseResponseWith(parserSelector: (contentType?: MIMEType) => DocumentParser) {
  return (r: Response) => {
    const ct = r.headers.get('content-type');
    let mt;
    if (ct) {
      mt = MIMEType.parse(ct) || undefined;
    }

    return parserSelector(mt)(r)
      .then(setContextUri(r))
      .then(rejectIfError);
  };
}

export const parseResponse = parseResponseWith(standardParserSelector);
