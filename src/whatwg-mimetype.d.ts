
declare module 'whatwg-mimetype' {

  type MIMETypeParameters = Map<string, string>;

  class MIMEType {

    type: string;
    subtype: string;
    essence: string;
    readonly parameters: MIMETypeParameters;

    static parse(s: string): MIMEType | null;

    constructor(s: string);

    isHTML(): boolean;
    isXML(): boolean;
    isJavaScript(opts?: { allowParameters?: boolean }): boolean;
  }

  export = MIMEType;
}
