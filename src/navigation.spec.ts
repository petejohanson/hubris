import fetch from 'isomorphic-fetch';
import * as jestFetchMock from 'jest-fetch-mock';
import URI from 'urijs';
import URITemplate from 'urijs/src/URITemplate';
import MIMEType from 'whatwg-mimetype';

import { find, follow, getFormFields, idMatch, query, relMatch } from './navigation';
import { Action, IData, IUberDocument } from './types';

/**
 * Types test
 */

describe('Navigation tests', () => {
  describe('inspecting the input parameters for data element', () => {
    describe('getFormFields', () => {
      describe('for an item with a templated URL', () => {
        it('returns basic data elements for a templated url', () => {
          const d: IData = { url: URITemplate('/items/{?page,count}') };

          expect(getFormFields(d)).toMatchObject([{ name: 'page' }, { name: 'count' }]);
        });

        it('handles multiple expressions with multiple variables in a templated URL', () => {
          const d: IData = { url: URITemplate('/items{/id,type}{?page,count}') };

          expect(getFormFields(d)).toMatchObject([
            { name: 'id' },
            { name: 'type' },
            { name: 'page' },
            { name: 'count' }
          ]);
        });

        describe('when there are child data elements with names matching variables', () => {
          it('includes matching child elements where possible', () => {
            const d: IData = { url: URITemplate('/items/{?page,count}'), data: [{ name: 'page', label: 'Page #' }] };

            expect(getFormFields(d)).toMatchObject([{ name: 'page', label: 'Page #' }, { name: 'count' }]);
          });
        });
      });

      describe('for an item with a templated model', () => {
        it('handles multiple expressions with multiple variables', () => {
          const d: IData = { model: URITemplate('{?name,dob}&type=user{&address}') };

          expect(getFormFields(d)).toMatchObject([{ name: 'name' }, { name: 'dob' }, { name: 'address' }]);
        });

        describe('when there are child data elements with names matching variables', () => {
          it('includes matching child elements where possible', () => {
            const d: IData = {
              model: URITemplate('{?page,count}'),
              data: [{ rel: ['profile'], url: URI('/profile.xml') }, { name: 'page', label: 'Page #' }]
            };

            expect(getFormFields(d)).toMatchObject([{ name: 'page', label: 'Page #' }, { name: 'count' }]);
          });
        });
      });
    });
  });
  describe('Following a data element', () => {
    beforeEach(jestFetchMock.resetMocks);

    it('Rejects if there is no URL in the data element', async () => {
      return expect(follow({})).rejects.toMatchObject(Error("'url' property is required to follow an IData element"));
    });

    it('Does a basic HTTP GET for a simple URL data element', () => {
      const emptyDocument = '{"uber":{}}';
      const mockRequest = jestFetchMock.mockResponseOnce(emptyDocument);

      const data = { url: new URI('http://localhost/') };

      return follow(data).then(r => {
        expect(r).toMatchObject({ body: emptyDocument });
        expect(mockRequest).toBeCalledWith('http://localhost/', {});
      });
    });

    it('properly resolves a templated link for the request url', () => {
      const emptyDocument = '{"uber":{}}';
      const mockRequest = jestFetchMock.mockResponseOnce(emptyDocument);

      const data = { url: new URITemplate('http://localhost/{?q}') };

      return follow(data, { q: 'cat gifs' }).then(r => {
        expect(r).toMatchObject({ body: emptyDocument });
        expect(mockRequest).toBeCalledWith('http://localhost/?q=cat%20gifs', {});
      });
    });

    it('properly resolves a templated link for the request url with fields input', () => {
      const emptyDocument = '{"uber":{}}';
      const mockRequest = jestFetchMock.mockResponseOnce(emptyDocument);

      const data = { url: new URITemplate('http://localhost/{?q}') };

      return follow(data, [{ name: 'q', value: 'cat gifs' }, { name: 'other' }]).then(r => {
        expect(r).toMatchObject({ body: emptyDocument });
        expect(mockRequest).toBeCalledWith('http://localhost/?q=cat%20gifs', {});
      });
    });

    it('properly resolves a templated link for the request even with no context data', () => {
      const emptyDocument = '{"uber":{}}';
      const mockRequest = jestFetchMock.mockResponseOnce(emptyDocument);

      const data = { url: new URITemplate('http://localhost/{?q}') };

      return follow(data).then(r => {
        expect(r).toMatchObject({ body: emptyDocument });
        expect(mockRequest).toBeCalledWith('http://localhost/', {});
      });
    });

    it('Respects the action of the element', () => {
      const mock = jestFetchMock.mockResponseOnce('{"uber": {}}');

      const data = { url: new URI('http://localhost/'), action: Action.Remove };

      return follow(data).then(_r =>
        expect(mock).toHaveBeenCalledWith('http://localhost/', { method: 'DELETE' })
      );
    });

    it('Uses the `accepting` property in the request `Accept` header', () => {
      const mock = jestFetchMock.mockResponseOnce('{"uber": {}}');

      const data = { url: new URI('http://localhost/'), accepting: [new MIMEType('image/*'), new MIMEType('text/*')] };

      return follow(data).then(_r =>
        expect(mock).toHaveBeenCalledWith('http://localhost/', {
          headers: new Headers({ Accept: 'image/*, text/*' })
        })
      );
    });

    it('Resolves the final URI using the context URI, if present', () => {
      const mock = jestFetchMock.mockResponseOnce('{"uber": {}}');

      const data = { url: new URI('/people'), document: { contextUri: new URI('http://localhost/') } };

      return follow(data).then(_r => expect(mock).toHaveBeenCalledWith('http://localhost/people', {}));
    });

    /* https://github.com/uber-hypermedia/specification/blob/master/includes/uber-request-body.txt */
    it('creates a HTTP request body by processing the model for requests', () => {
      const mock = jestFetchMock.mockResponseOnce('{"uber": {}}');

      const data = {
        name: 'create',
        rel: ['http://example.org/rels/create'],
        url: new URI('http://example.org/people/'),
        action: Action.Append,
        model: new URITemplate('g={givenName}&f={familyName}&e={email}&a={avatarUrl}')
      };

      const input = {
        givenName: 'Mike',
        familyName: 'Amundsen',
        email: 'mike@example.org',
        avatarUrl: 'http://example.org/avatars/mike.png'
      };

      const expectedBody = 'g=Mike&f=Amundsen&e=mike%40example.org&a=http%3A%2F%2Fexample.org%2Favatars%2Fmike.png';

      const expectedRequestInit = { method: 'POST', headers: new Headers({
          'Content-Type': 'application/x-www-form-urlencoded'
        }), body: expectedBody };

      return follow(data, input).then(_r =>
        expect(mock).toHaveBeenCalledWith('http://example.org/people/', expectedRequestInit)
      );
    });

    it('uses the model for the request URI for Read action', () => {
      const data = {
        url: new URI('https://www.google.com/'),
        model: new URITemplate('{?q}')
      };

      const mock = jestFetchMock.mockResponseOnce('{"uber": {}}');


      const input = { q: 'cat' };

      return follow(data, input).then(_r =>
        expect(mock).toHaveBeenCalledWith('https://www.google.com/?q=cat', {})
      );
    });
  });

  describe('querying for a data element', () => {
    it('can find an immediate child by ID', () => {
      const doc = {
        data: [
          { id: 'search', url: new URITemplate('http://localhost:/{?q}') }
        ]
      };

      const found = query('search')(doc);

      expect(found).toEqual({ id: 'search', url: new URITemplate('http://localhost:/{?q}') });
    });

    it('can query a hierarchy', () => {
      const doc = {
        data: [
          {
            id: 'search',
            url: new URITemplate('http://localhost:/{?q}'),
            data: [
              { id: 'q', label: 'Search Term', value: 'Default Search' }
            ]
          }
        ]
      };

      const found = query('search', 'q')(doc);

      expect(found).toEqual({ id: 'q', label: 'Search Term', value: 'Default Search' });
    });

    it('can query a hierarchy using explicit idMatch', () => {
      const doc = {
        data: [
          {
            id: 'search',
            url: new URITemplate('http://localhost:/{?q}'),
            data: [
              { id: 'q', label: 'Search Term', value: 'Default Search' }
            ]
          }
        ]
      };

      const found = query(idMatch('search'), idMatch('q'))(doc);

      expect(found).toEqual({ id: 'q', label: 'Search Term', value: 'Default Search' });
    });

    it('returns undefined when the search fails to find a child', () => {
      const doc = {
        data: [
          {
            id: 'search',
            url: new URITemplate('http://localhost:/{?q}'),
            data: [
              { id: 'q', label: 'Search Term', value: 'Default Search' }
            ]
          }
        ]
      };

      expect(query('foo')(doc)).toBeUndefined();
    });

    it('can query on rel', () => {
      const doc = {
        data: [
          {
            rel: ['search', 'query'],
            url: new URITemplate('http://localhost:/{?q}'),
            data: [
              { id: 'q', label: 'Search Term', value: 'Default Search' }
            ]
          }
        ]
      };

      const found = query(relMatch('search'), 'q')(doc);

      expect(found).toEqual({ id: 'q', label: 'Search Term', value: 'Default Search' });
    });

    it('returns undefined when the search fails deeper in the chain', () => {
      const doc = {
        data: [
          {
            id: 'search',
            url: new URITemplate('http://localhost:/{?q}'),
            data: [
              { id: 'q', label: 'Search Term', value: 'Default Search' }
            ]
          }
        ]
      };

      expect(query('search', 'search')(doc)).toBeUndefined();
    });

    it('returns undefined when the search fails for an empty document', () => {
      const doc = {
        version: '1.0'
      };

      expect(query('search', 'search')(doc)).toBeUndefined();
    });
  });

  describe('finding an element', () => {
    describe('by string id', () => {
      it('finds a toplevel element', () => {
        const doc = {
          data: [
            { id: 'search', url: new URITemplate('http://localhost:/{?q}') }
          ]
        };

        const found = find('search')(doc);

        expect(found).toEqual({ id: 'search', url: new URITemplate('http://localhost:/{?q}') });
      });

      it('find a deeply nested data element', () => {
        const doc = {
          data: [
            {
              rel: ['self'],
              url: new URITemplate('http://localhost:/{?q}')
            },
            {
              id: 'search',
              url: new URITemplate('http://localhost:/{?q}'),
              data: [
                { id: 'q', label: 'Search Term', value: 'Default Search' }
              ]
            }
          ]
        };

        const found = find('q')(doc);

        expect(found).toEqual({ id: 'q', label: 'Search Term', value: 'Default Search' });
      });

      it('returns the element when searching by relMatch', () => {
        const doc = {
          data: [
            {
              id: 'search',
              url: new URITemplate('http://localhost:/{?q}'),
              data: [
                { id: 'q', label: 'Search Term', value: 'Default Search' }
              ]
            },
            {
              rel: ['self'],
              url: new URITemplate('http://localhost/')
            }
          ]
        };

        const found = find(relMatch('self'))(doc);

        expect(found).toEqual({ rel: ['self'], url: new URITemplate('http://localhost/') });
      });

      it('returns undefined when failing to a deeply nested data element', () => {
        const doc = {
          data: [
            {
              rel: ['self'],
              url: new URITemplate('http://localhost:/{?q}')
            },
            {
              id: 'search',
              url: new URITemplate('http://localhost:/{?q}'),
              data: [
                { id: 'q', label: 'Search Term', value: 'Default Search' }
              ]
            }
          ]
        };

        const found = find('q2')(doc);

        expect(found).toBeUndefined();
      });

      it('returns undefined when searching an empty document', () => {
        const doc = { };

        const found = find('q')(doc);
        expect(found).toBeUndefined();
      });
    });
  });
});
