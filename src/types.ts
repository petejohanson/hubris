import parseXml from '@rgrove/parse-xml';
import * as uri from 'urijs';
import URITemplate from 'urijs/src/URITemplate';
import MIMEType from 'whatwg-mimetype';

export enum Action {
  Append = 'append',
  Partial = 'partial',
  Read = 'read',
  Remove = 'remove',
  Replace = 'replace'
}

export enum Transclude {
  True = 'true',
  False = 'false',
  Audio = 'audio',
  Image = 'image',
  Text = 'text',
  Video = 'video'
}

export class UberError extends Error {
  constructor(document: IUberDocument) {
    super('The document includes error data');

    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(this, UberError.prototype);
    }
  }
}

export interface IDataParent {
  data?: IData[];
}

export interface IData extends IDataParent {
  id?: string;
  name?: string;
  label?: string;
  rel?: string[];
  url?: uri.URI | uri.URITemplate;
  model?: uri.URITemplate;
  action?: Action;
  value?: string | number | boolean;
  transclude?: Transclude;
  accepting?: MIMEType[];
  sending?: MIMEType[];

  // The containing document
  document?: IUberDocument;
}

/**
 * @description The Uber document
 */
export interface IUberDocument extends IDataParent {
  version?: string;
  error?: IDataParent;

  // Extra context
  contextUri?: uri.URI;
}
