
import { Action, IData, IDataParent, IUberDocument } from './types';

import 'isomorphic-fetch';
import * as uri from 'urijs';
import URITemplate from 'urijs/src/URITemplate';

/**
 * @description Get the URL for a data element, with optional URI Template input values.
 * @param data The element from which to get/construct the URL
 * @param context The URI Template input values for resolving templated `url` values.
 */
export function getUrl(data: IData, context?: uri.URITemplateInput) {
  if (!data.url) {
    return null;
  }

  let url = data.url instanceof URITemplate ? data.url.expand(context || {}) : data.url;

  if (data.document && data.document.contextUri) {
    url = url.absoluteTo(data.document.contextUri);
  }

  if (data.model && (!data.action || data.action === Action.Read)) {
    url.query(data.model.expand(context || {}).toString());
  }

  return url.toString();
}

export function getFormFields(data: IData) {
  const dataMap = new Map<string, IData>();
  if (data.data) {
    data.data.forEach(c => {
      const { name } = c;
      if (name) {
        dataMap.set(name, c);
      }
    });
  }

  const fields: IData[] = [];

  function inspect(t: uri.URITemplate) {
    (t.parse().parts || []).forEach(p => {
      if ((p as uri.URITemplateExpression).expression) {
        const exp = p as uri.URITemplateExpression;
        exp.variables.forEach(({ name }) => {
          fields.push(dataMap.get(name) || { name });
        });
      }
    });
  }

  if (data.url instanceof URITemplate) {
    inspect(data.url);
  }

  if (data.model) {
    inspect(data.model);
  }

  return fields;
}

const ACTION_TO_METHOD_MAP = {
  [Action.Append]: 'POST',
  [Action.Partial]: 'PATCH',
  [Action.Read]: 'GET',
  [Action.Remove]: 'DELETE',
  [Action.Replace]: 'PUT'
};

const dataToFetchInit = (data: IData, context: uri.URITemplateInput) => {
  let headers: Headers | undefined;
  const init: RequestInit = {};

  if (data.action) {
    init.method = ACTION_TO_METHOD_MAP[data.action];
  }

  if (data.model && data.action && data.action !== Action.Read) {
    init.body = data.model.expand(context).toString();

    headers = new Headers();
    headers.set('Content-Type', 'application/x-www-form-urlencoded');
  }

  if (data.accepting) {
    if (!headers) {
      headers = new Headers();
    }

    headers.set('Accept', data.accepting.join(', '));
  }

  if (headers) {
    init.headers = headers;
  }

  return init;
};

function convertFormFieldsToURITemplateInput(fields: IData[]) {
  return fields.reduce<{ [key: string]: string }>((o, v) => {
    if (v.name && v.value) {
      o[v.name] = v.value.toString();
    }
    return o;
  }, {});
}

/**
 * Initiate a network request using the given data element and optional
 * URI template input data to form the request. Allows injecting your own
 * whatwg-fetch compatible fetch function, allowing for wrappers for the core
 * fetch to include authentication, default headers, etc.
 *
 * @param f The fetch compatible function to perform HTTP requests
 */
export function followWith(f: (input: RequestInfo, init?: RequestInit | undefined) => Promise<Response>) {
  return (data: IData, context?: uri.URITemplateInput | IData[]): Promise<Response> => {
    if (context && context instanceof Array) {
      context = convertFormFieldsToURITemplateInput(context);
    }
    const url = getUrl(data, context);

    if (!url) {
      return Promise.reject(new Error("'url' property is required to follow an IData element"));
    }

    return f(url, dataToFetchInit(data, context || {}));
  };
}

/**
 * Follow an Uber data element using the default whatwg-fetch global function.
 */
export const follow = followWith(fetch);

export type DataElementMatcher = (data: IData) => boolean;
export type QueryTerm = string | DataElementMatcher;

/**
 * @description A matcher to query for an Uber data element that has a certain `id` value.
 * @param id The ID to check for in the Uber `id` field.
 */
export function idMatch(id: string): DataElementMatcher {
  return (data: IData) => data.id === id;
}

/**
 * @description A matcher to query for an Uber data element that contains a certain `rel` value.
 * @param rel The relationship to look for being included in the Uber `rel` array field.
 */
export function relMatch(rel: string): DataElementMatcher {
  return (data: IData) => (data.rel || []).includes(rel);
}

const toMatch = (term: QueryTerm) => (typeof term === 'string' ? idMatch(term) : term);

export function query(...args: QueryTerm[]) {
  const f: (doc: IUberDocument | IDataParent) => IData | undefined = (doc: IUberDocument | IDataParent) => {
    let parent = doc as IDataParent;

    if (!parent || !parent.data) {
      return undefined;
    }

    let items = args.map(toMatch);

    do {
      const [item, ...rest] = items;
      items = rest;

      parent = parent.data.find(item) as IDataParent;
    } while (items.length > 0 && parent && parent.data);

    return parent;
  };

  return f;
}

/**
 * @description Perform a depth-first search for a data element based on a query term
 */
export function find(term: QueryTerm) {
  const match = toMatch(term);

  return (element: IDataParent) => {
    const candidates = Array.from(element.data || []);

    do {
      const item = candidates.shift();

      if (!item) {
        break;
      }

      if (match(item)) {
        return item;
      }

      if (item.data && item.data.length) {
        Array.prototype.splice.apply(candidates, [0 as any, 0].concat(item.data));
      }
    } while (true);

    return undefined;
  };
}
